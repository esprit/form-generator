<?php

namespace FormGenerator\Configuration;

use Attribute;

#[Attribute]
class Field
{
    public function __construct(
        public string $type,
        public array $options = [],
    ) {}
}
