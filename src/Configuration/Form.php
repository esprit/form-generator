<?php

namespace FormGenerator\Configuration;

use Attribute;

#[Attribute]
class Form
{
    public function __construct(
        public string $name
    ) {}
}
