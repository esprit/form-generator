<?php

namespace FormGenerator\TypeGuesser;

use ReflectionNamedType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormTypeGuesserInterface;
use Symfony\Component\Form\Guess\Guess;
use Symfony\Component\Form\Guess\TypeGuess;

class PropertyTypeGuesser implements FormTypeGuesserInterface
{
    public function guessType(string $class, string $property): ?TypeGuess
    {
        $type = $this->readTypeOnProperty($class, $property);

        if (! $type) {
            return null;
        }

        switch ($type) {
            //case 'string':
            //    return new TypeGuess(TextType::class, array(), Guess::HIGH_CONFIDENCE);

            case 'float':
            case 'double':
            case 'real':
            case 'int':
            case 'integer':
                return new TypeGuess(NumberType::class, array(), Guess::HIGH_CONFIDENCE);

            case 'boolean':
            case 'bool':
                return new TypeGuess(CheckboxType::class, array(), Guess::HIGH_CONFIDENCE);
        }

        return  null;
    }

    private function readTypeOnProperty(string $class, string $property):  ?string
    {
        $reflectionClass = new \ReflectionClass($class);
        if (! $reflectionClass->hasProperty($property)) {
            return null;
        }

        $reflectionProperty = new \ReflectionProperty($class, $property);
        $type =  $reflectionProperty->getType();

        if ($type instanceof ReflectionNamedType) {
            return $type->getName();
        }

        return null;
    }

    public function guessRequired(string $class, string $property)
    {
    }

    public function guessMaxLength(string $class, string $property)
    {
    }

    public function guessPattern(string $class, string $property)
    {
    }
}
