<?php

namespace FormGenerator\TypeGuesser;

use FormGenerator\Configuration\Field;
use Symfony\Component\Form\FormTypeGuesserInterface;
use Symfony\Component\Form\Guess\Guess;
use Symfony\Component\Form\Guess\TypeGuess;
use Symfony\Component\Form\Guess\ValueGuess;

class FieldAttributeGuesser implements FormTypeGuesserInterface
{
    public function guessType(string $class, string $property): ?TypeGuess
    {
        $field = $this->fieldAnnotation($class, $property);

        if (null === $field) {
            return null;
        }

        return new TypeGuess($field->type, $field->options, Guess::VERY_HIGH_CONFIDENCE);
    }

    private function fieldAnnotation(string $class, string $property): ?object
    {
        $reflectionClass = new \ReflectionClass($class);
        if (! $reflectionClass->hasProperty($property)) {
            return null;
        }

        $refProperty = $reflectionClass->getProperty($property);
        $attributes = $refProperty->getAttributes(Field::class);

        if (! $attributes) {
            return null;
        }

        return $attributes[0]->newInstance();
    }

    /**
     * @inheritdoc
     */
    public function guessRequired(string $class, string $property): ?ValueGuess
    {
        $field = $this->fieldAnnotation($class, $property);

        if (null === $field) {
            return null;
        }

        if (isset($field->options['required'])) {
            return new ValueGuess($field->options['required'], Guess::VERY_HIGH_CONFIDENCE);
        }

        return null;
    }

    public function guessMaxLength(string $class, string $property)
    {
    }

    public function guessPattern(string $class, string $property)
    {
    }
}
