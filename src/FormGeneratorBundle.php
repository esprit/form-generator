<?php

namespace FormGenerator;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FormGeneratorBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new FormGeneratorExtension();
    }    
}
