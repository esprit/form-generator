<?php

namespace FormGenerator\Generator;

use FormGenerator\Configuration\Form;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class FormGenerator
{
    public function __construct(private FormFactoryInterface $formFactory)
    {
    }

    /** @param class-string $class */
    public function getFormConfig(string $class): object
    {
        $refClass = new \ReflectionClass($class);
        $attributes =  $refClass->getAttributes(Form::class);
        return $attributes[0]->newInstance();
    }

    public function create(object $formData, array $options = []) : FormInterface
    {
        return $this->createBuilder($formData, $options)->getForm();
    }

    public function createBuilder(object $formData, array $options = [], string $name = null) : FormBuilderInterface
    {
        $class = get_class($formData);

        if (!$name) {
            // read name from attribute
            $name = $this->getFormConfig($class)->name;
        }

        $builder = isset($name)
            ? $this->formFactory->createNamedBuilder($name, FormType::class, $formData, $options)
            : $this->formFactory->createBuilder(FormType::class, $formData, $options);

        // TODO implement Embedded back
        foreach ((new \ReflectionClass($class))->getProperties() as $property) {
            $builder->add($property->getName());
        }

        return $builder;
    }
}
