<?php

namespace FormGenerator\Traits;

trait DataMapper
{
    public function export($object, array $skipFields = [])
    {
        foreach (array_keys(get_class_vars($object::class)) as $key) {
            if (in_array($key, $skipFields)) {
                continue;
            }
            if (property_exists($this, $key)) {
                $object->$key = $this->$key;
            }
        }

        return $object;
    }

    public function import($object, array $skipFields = []): void
    {
        foreach (get_object_vars($object) as $key => $value) {
            if (in_array($key, $skipFields)) {
                continue;
            }
            $this->$key = $value;
        }
    }
}